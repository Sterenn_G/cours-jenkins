package com.formation.epsi.java.superherogestion.model;


import javax.persistence.*;
import java.util.List;


@Entity
@Table(name= "vilain")
public class Vilain {

    @ManyToMany (cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
    @JoinTable( 
        name = "super_hero_vilain",
        joinColumns = {@JoinColumn (name = "super_hero_id")},
        inverseJoinColumns = {@JoinColumn (name = "vilain_id")}
        )
    private List <Vilain> vilain;

public List<Vilain> getVilain() {
        return vilain;
    }



    public void setVilain(List<Vilain> vilain) {
        this.vilain = vilain;
    }

@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
private long id;

@Column(name = "vilain_name", nullable = false)
private String vilainName;

public Vilain() {

}


public long getId() {
    return id;
}

public void setId(long id) {
    this.id = id;
}

public String getVilainName() {
    return vilainName;
}

public void setVilainName(String vilainName) {
    this.vilainName = vilainName;
}
}
