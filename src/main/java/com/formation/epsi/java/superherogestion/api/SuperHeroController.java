package com.formation.epsi.java.superherogestion.api;


import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import java.util.List;
import java.util.stream.Collectors;

import com.formation.epsi.java.superherogestion.api.dto.SuperHeroDTO;
import com.formation.epsi.java.superherogestion.model.SuperHeros;
import com.formation.epsi.java.superherogestion.repositories.SuperHeroRepository;

@RestController
@RequestMapping(
        path = "/superHeroes",
        produces = {MediaType.APPLICATION_JSON_VALUE}
)
public class SuperHeroController {

private final SuperHeroRepository superHeroRepository;

    SuperHeroController(
            SuperHeroRepository superHeroRepository
    ) {
        this.superHeroRepository = superHeroRepository;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SuperHeroDTO> create(@RequestBody SuperHeroDTO superHeroDTO) {
        superHeroDTO.setId(0);
        SuperHeros superHero = mapToEntity(superHeroDTO);
        return ResponseEntity
        .status(HttpStatus.CREATED)
        .body(mapToDTO(this.superHeroRepository.save(superHero)));
    }

    @GetMapping
    public ResponseEntity<List<SuperHeroDTO>> getAll() {
//         List<SuperHero> superHeroes = this.superHeroRepository.findAll();
//         List<SuperHeroDTO> superHeroDTOS = new ArrayList<>();
//         superHeroes.forEach(superHero -> superHeroDTOS.add(mapToDTO(superHero)));
//
//        return ResponseEntity.ok(superHeroDTOS);
        return ResponseEntity.ok(
                this.superHeroRepository
                        .findAll()
                        .stream()
                        .map(this::mapToDTO)
                        .collect(Collectors.toList())
        );
    }

    private SuperHeroDTO mapToDTO(SuperHeros superHero) {
        return new SuperHeroDTO( superHero.getId(), superHero.getSuperHeroName(), superHero.getSecretIdentity());
    }

    private SuperHeros mapToEntity(SuperHeroDTO superHeroDTO) {
            SuperHeros superHero = new SuperHeros();
                superHero.setSecretIdentity(superHeroDTO.getSecretIdentity());
                superHero.setSuperHeroName(superHeroDTO.getSuperHeroName());
                superHero.setId(superHeroDTO.getId());
        return superHero;
    }
}

