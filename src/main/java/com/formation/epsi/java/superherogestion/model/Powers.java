package com.formation.epsi.java.superherogestion.model;



import javax.persistence.*;
import java.util.List;


@Entity
@Table(name= "powers")
public class Powers {
    @ManyToMany (mappedBy = "powers")
    List <SuperHeros> superHero;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "power_name", nullable = false)
    private String powerString;

    @Column(name = "power_description", nullable = false)
    private String descriptionString;

    
public Powers() {

}


public List<SuperHeros> getSuperHero() {
    return superHero;
}


public void setSuperHero(List<SuperHeros> superHero) {
    this.superHero = superHero;
}


public long getId() {
    return id;
}


public void setId(long id) {
    this.id = id;
}


public String getPowerString() {
    return powerString;
}


public void setPowerString(String powerString) {
    this.powerString = powerString;
}


public String getDescriptionString() {
    return descriptionString;
}


public void setDescriptionString(String descriptionString) {
    this.descriptionString = descriptionString;
}
}
