package com.formation.epsi.java.superherogestion.model;


import javax.persistence.*;

import java.util.List;

@Entity
@Table(name= "super_hero")
public class SuperHeros {

    @ManyToMany (cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
    @JoinTable( 
        name = "super_hero_powers",
        joinColumns = {@JoinColumn (name = "super_hero_id")},
        inverseJoinColumns = {@JoinColumn (name = "powers_id")}
        )
    private List <Powers> powers;

    @ManyToMany (cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
    private List <Vilain> vilain;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "super_hero_name", nullable = false)
    private String superHeroName;

    @Column(name = "secret_identity", nullable = false)
    private String secretIdentity;

    public SuperHeros() {
    }

    public List<Powers> getPowers() {
        return powers;
    }

    public void setPowers(List<Powers> powers) {
        this.powers = powers;
    }

    public List<Vilain> getVilain() {
        return vilain;
    }

    public void setVilain(List<Vilain> vilain) {
        this.vilain = vilain;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSuperHeroName() {
        return superHeroName;
    }

    public void setSuperHeroName(String superHeroName) {
        this.superHeroName = superHeroName;
    }

    public String getSecretIdentity() {
        return secretIdentity;
    }

    public void setSecretIdentity(String secretIdentity) {
        this.secretIdentity = secretIdentity;
    }

    
}
