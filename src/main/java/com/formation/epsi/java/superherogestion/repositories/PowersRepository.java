package com.formation.epsi.java.superherogestion.repositories;



import com.formation.epsi.java.superherogestion.model.Powers;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PowersRepository extends JpaRepository<Powers, Long> {
}


