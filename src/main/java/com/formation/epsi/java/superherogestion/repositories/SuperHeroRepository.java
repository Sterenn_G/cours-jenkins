package com.formation.epsi.java.superherogestion.repositories;

import com.formation.epsi.java.superherogestion.model.SuperHeros;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SuperHeroRepository extends JpaRepository<SuperHeros, Long> {
}
